FROM node:12.21-buster
WORKDIR /node_app
COPY package*.json /node_app/
RUN npm install
RUN npm install pm2 -g
COPY . /node_app/
EXPOSE 3000
CMD  [ "pm2-runtime","start","deploy.json" ]
